packagesPacman=(
    "emacs-wayland"
    "firefox-i18n-zh-cn"
    "firefox-ublock-origin"
    "freerdp"
    "remmina"
    "noto-fonts-cjk"
    "noto-fonts-extra"
    "noto-fonts-emoji"
    "wqy-microhei"
    "wqy-microhei-lite"
    "wqy-bitmapfont"
    "wqy-zenhei"
    "otf-font-awesome"
    "ttf-fira-sans"
    "ttf-fira-code"
    "ttf-firacode-nerd"
    "adobe-source-code-pro-fonts"
    "adobe-source-sans-fonts"
    "adobe-source-serif-fonts"
    "adobe-source-han-sans-cn-fonts"
    "adobe-source-han-sans-hk-fonts"
    "adobe-source-han-sans-tw-fonts"
    "adobe-source-han-serif-cn-fonts"
    "thunar-nextcloud-plugin"
    "thunar-vcs-plugin"
    "fcitx5"
    "fcitx5-nord"
    "fcitx5-configtool"
    "fcitx5-gtk"
    "fcitx5-mozc"
    "fcitx5-pinyin-zhwiki"
    "fcitx5-material-color"
    "fcitx5-rime"
    "librime"
    "rar"
    "zathura"
    "zathura-pdf-poppler"
    "xreader"
    "ranger"
    "graphviz"
    "qemu-full"
    "libvirt"
    "virt-manager"
    "ebtables"
    "dnsmasq"
    "bridge-utils"
    "openbsd-netcat"
    "htop"
    "imv"
);

packagesYay=(
    "bibata-cursor-theme"
    "fcitx5-skin-adwaita-dark"
    "obs-studio"
    "xorg-xlsclients"
    "pomatez"
    "nemo-folder-icons"
);
