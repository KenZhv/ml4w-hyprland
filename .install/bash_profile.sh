# ------------------------------------------------------
# Install .bash_profile
# ------------------------------------------------------

echo -e "${GREEN}"
figlet ".bash_profile"
echo -e "${NONE}"
if [ ! -L ~/.bash_profile ] && [ -f ~/.bash_profile ]; then
    echo "PLEASE NOTE: The script has detected an existing .bash_profile file."
fi
if [ -f ~/dotfiles-versions/backups/$datets/.bash_profile-old ]; then
    echo "Backup is already available here ~/dotfiles-versions/backups/$datets/.bash_profile-old"
fi
if [ ! -L ~/.bash_profile ] && [ -f ~/.bash_profile ]; then
    bash_confirm="Do you want to replace your existing .bash_profile file with the HL4W dotfiles .bash_profile file?"
    if gum confirm "$bash_confirm" ;then
        rm ~/.bash_profile
        _installSymLink .bash_profile ~/.bash_profile ~/dotfiles/.bash_profile ~/.bash_profile
    elif [ $? -eq 130 ]; then
            exit 130
    else
        echo "Installation of the .bash_profile file skipped."
    fi
else
    bash_confirm="Do you want to install the HL4W dotfiles .bash_profile file now?"
    if gum confirm "$bash_confirm" ;then
        if [ -L ~/.bash_profile ] || [ -f ~/.bash_profile ]; then
            rm ~/.bash_profile
            echo "Existing .bash_profile removed."
        fi
        _installSymLink .bash_profile ~/.bash_profile ~/dotfiles/.bash_profile ~/.bash_profile
    elif [ $? -eq 130 ]; then
            exit 130
    else
        echo "Installation of the .bash_profile file skipped."
    fi
fi
echo ""
