# ------------------------------------------------------
# Check if yay is installed
# ------------------------------------------------------
echo -e "${GREEN}"
figlet "yay"
echo -e "${NONE}"
if sudo pacman -Qs yay > /dev/null ; then
    echo ":: yay is already installed!"
else
    echo ":: yay is not installed. Starting the installation!"
    _installPackagesPacman "base-devel"
    echo ":: install yay using archlinuxcn."
    # or using bash method
    # sudo bash -c 'echo -e "[archlinuxcn] \nServer = https://mirrors.ustc.edu.cn/archlinuxcn/\$arch" >> /etc/pacman.conf'
    
    echo -e "[archlinuxcn] \nServer = https://mirrors.ustc.edu.cn/archlinuxcn/\$arch" | sudo tee -a /etc/pacman.conf > /dev/null
    sudo pacman-key --lsign-key "farseerfc@archlinux.org"
    sudo pacman -Sy archlinuxcn-keyring --noconfirm
    sudo pacman -Suyy --noconfirm
    sudo pacman -S yay  --noconfirm
    echo ":: yay has been installed successfully."
fi
echo ""